#Generador de datos de una distribución exponencial con párametro tau = 2
import ROOT
import time
r = ROOT.TRandom(0) 					# Generador de numeros al azar
h1 = ROOT.TH1D('h1','expo',20,0,20)		# Genera un histograma unidimencional y se guardan doubles

for x in range(0,40):					# Con el for se van llenando las columnas del histograma
	x = r.Exp(2)
	print(x)
	h1.Fill(x)							# Llena el histograma

h1.Fit('expo')							# Fitea el histograma
h1.Fit('expo',"P+")
h1.Fit('expo',"L+")
#h1.Draw()
#time.sleep(10)