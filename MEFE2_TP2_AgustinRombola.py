''' 

AGUSTIN MARIANO ROMBOLA - ENTREGA 2

Parte A
 1. Generar n números aleatorios xi con distribucion exponencial con un
 valor dado de τ y calcular un estadístico t igual al promedio de los xi.
 2. Repetir el punto anterior, un numero NExperimentos de veces y guardar
 el valor del estadístico t obtenido cada vez en un histograma.
 3. Para ese histograma, encontrar un valor de tmin y tmax tal que entre
 ellos se encuentre una fracción CL del total de los eventos.
 4. Repetir los tres puntos anteriores para 100 valores de tau entre 0 y 10
 y graficar  el cinturón de confianza en el plano τ vs t.

Parte B
 Utilizar la herramienta generada en la Parte A para otros dos estadísticos:
 1. la mediana 
 2. t=Σi  (xi+xi**2+xi**3+xi**4) ( Use t=ln( Σ xi+xi**2+xi**3) )

Parte C
Discutir que pasa si como "estadístico" t utilizamos el q de Wilks visto en clase. 

Parte D
Utilizar Wilks para calcular el intervalo de confianza para el parámetro τ de la exponencial con un dado CL.

Parte E
(Opcional) Calcular la cobertura de los intervalos calculados en los ítems anteriores en función del parámetro τ 
'''
##########################################
## LIBRERIAS
##########################################

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages

##########################################
## VARIABLES GLOBALES
##########################################

nToys = 5000
nPoints = 10
nTaus = 100
nBins = 50
CL = 0.68
taus = np.linspace(0,10,nTaus,endpoint=True)

##########################################
## FUNCIONES
##########################################

def findCL(xHist,yHist,CL,nPoints,tipo):
	#Busca el min
	if tipo == 'central':
		i = 0
		acum = 0
		while(acum < (1-CL)/2):
			acum = acum + float(yHist[i])/nPoints
			i += 1
		tMin = xHist[i]
		#Busca el max
		i = 0
		acum = 0
		while(acum < (1-(1-CL)/2)):
			acum = acum + float(yHist[i])/nPoints
			i += 1
		tMax = xHist[i]
	elif tipo == 'upper':
		i = 0
		acum = 0
		while(acum < (1-CL)):
			acum = acum + float(yHist[i])/nPoints
			i += 1
		tMax = xHist[i]
		tMin = xHist[0]
	return tMin,tMax

def estadistico_especial(x):
	t = 0
	for i in range(len(x)):
	#for i in range(4):
		t += x[i] + x[i]**2 + x[i]**3 #+ x[i]**4
	return np.log(t)

def LL_exp(x,t):
	beta = 1/t
	n = len(x)
	y = n*np.log(beta)-beta*np.sum(x)
	return y

def q_LL(x,t1,t2):
	y = -2*(LL_exp(x,t1) - LL_exp(x,t2))
	return y

def save_histogram(est,figs):
	fileName = f'hist_{est}.pdf'
	print('Graficando los histogramas...')
	pp = PdfPages(fileName)
	for i in figs:
		pp.savefig(i)
	pp.close()

##########################################
## MAIN FUNCTION
##########################################

def mainFunction(taus,nPoints,nToys,nTaus,nBins,CL,est,save_hist):
	confidenceBelt = {'tau':taus[1:],'tmin':[],'tmax':[]}
	figs = []
	count_hist = 5
	for tau in taus[1:]:
		estadistico = []
		print(f'Calculando para un tau = {tau} con {est}')
		for i in range(nToys):
			x = []
			for j in range(nPoints):
				x.append(np.random.exponential(scale=tau))
			if est == 'promedio':
				estadistico.append(np.mean(x))
				tipo = 'central'
			if est == 'mediana':
				estadistico.append(np.median(x))
				tipo = 'central'
			if est == 'especial':
				estadistico.append(estadistico_especial(x))
				tipo = 'central'
			if est == 'likelihood':
				estadistico.append(q_LL(x,tau,np.mean(x)))
				tipo = 'upper' #Porque es una chicuadrado con un grado de libertad

		#Arma los datos para el histograma
		xHist = np.linspace(np.min(estadistico),np.max(estadistico),nBins)
		yHist,a = np.histogram(estadistico,xHist,normed=False)
		widthBar = xHist[1]-xHist[0]

		#Busca el intervalo de confianza para tau 
		tMin,tMax = findCL(xHist,yHist,CL,nToys,tipo)

		#Para guardar los histogramas en un pdf genera las figuras
		if save_hist:
			fig_aux = plt.figure(count_hist)
			plt.title(f'Tau = {tau}')
			plt.bar(xHist[:-1],yHist,width=widthBar,edgecolor='black',linewidth=1)
			plt.axvline(x=tMin,color='red')
			plt.axvline(x=tMax,color='red')
			figs.append(fig_aux)
			count_hist += 1
		
		#Guarda el intervalo de confianza para ir armando el cofidence belt
		confidenceBelt['tmin'].append(tMin)
		confidenceBelt['tmax'].append(tMax)

	#Guarda los histogramas en un pdf
	if save_hist:
		save_histogram(est,figs)
	return confidenceBelt

##########################################
## MAIN PROGRAM
##########################################

confidenceBelt_promedio = mainFunction(taus,nPoints,nToys,nTaus,nBins,CL,'promedio',False)
confidenceBelt_mediana = mainFunction(taus,nPoints,nToys,nTaus,nBins,CL,'promedio',False)
confidenceBelt_especial = mainFunction(taus,nPoints,nToys,nTaus,10,CL,'especial',False)
confidenceBelt_likelihood = mainFunction(taus,nPoints,nToys,nTaus,nBins,CL,'likelihood',True)

##########################################
## GRAFICOS
##########################################

# 1) PROMEDIO
titulo = f'G1) Cofidence Belt (Promedio) - nToys = {nToys}, nPoints = {nPoints}, nTaus = {nTaus}'
fig1 = plt.figure(1)
plt.title(titulo)
plt.plot(confidenceBelt_promedio['tau'],confidenceBelt_promedio['tmin'],color='red',label='tMin')
plt.plot(confidenceBelt_promedio['tau'],confidenceBelt_promedio['tmax'],color='red',label='tMan')
plt.fill_between(confidenceBelt_promedio['tau'],confidenceBelt_promedio['tmin'],confidenceBelt_promedio['tmax'],color='#66CC00',label=f'Confidence Level {CL}')
#plt.plot(confidenceBelt_promedio['tau'],confidenceBelt_promedio['tau'],label='Real value')
plt.legend()
#plt.show()

# 2) MEDIANA
titulo = f'G2) Cofidence Belt (Median) - nToys = {nToys}, nPoints = {nPoints}, nTaus = {nTaus}'
fig2 = plt.figure(2)
plt.title(titulo)
plt.plot(confidenceBelt_mediana['tau'],confidenceBelt_mediana['tmin'],color='red',label='tMin')
plt.plot(confidenceBelt_mediana['tau'],confidenceBelt_mediana['tmax'],color='red',label='tMan')
plt.fill_between(confidenceBelt_mediana['tau'],confidenceBelt_mediana['tmin'],confidenceBelt_mediana['tmax'],color='#66CC00',label=f'Confidence Level {CL}')
#plt.plot(confidenceBelt_mediana['tau'],confidenceBelt_mediana['tau'],label='Real value')
plt.legend()
#plt.show()

# 3) ESPECIAL
titulo = f'G3) Cofidence Belt (Especial) - nToys = {nToys}, nPoints = {nPoints}, nTaus = {nTaus}'
fig3 = plt.figure(3)
plt.title(titulo)
plt.yscale('log')
plt.plot(confidenceBelt_especial['tau'],confidenceBelt_especial['tmin'],color='red',label='tMin')
plt.plot(confidenceBelt_especial['tau'],confidenceBelt_especial['tmax'],color='red',label='tMan')
plt.fill_between(confidenceBelt_especial['tau'],confidenceBelt_especial['tmin'],confidenceBelt_especial['tmax'],color='#66CC00',label=f'Confidence Level {CL}')
#plt.plot(confidenceBelt_especial['tau'],confidenceBelt_especial['tau'],label='Real value')
plt.legend()
#plt.show()

# 4) CON LIKELIHOOD
titulo = f'G4) Cofidence Belt (Likelihood) - nToys = {nToys}, nPoints = {nPoints}, nTaus = {nTaus}'
fig4 = plt.figure(4)
plt.title(titulo)
plt.plot(confidenceBelt_likelihood['tau'],confidenceBelt_likelihood['tmin'],color='red',label='tMin')
plt.plot(confidenceBelt_likelihood['tau'],confidenceBelt_likelihood['tmax'],color='red',label='tMan')
plt.fill_between(confidenceBelt_likelihood['tau'],confidenceBelt_likelihood['tmin'],confidenceBelt_likelihood['tmax'],color='#66CC00',label=f'Confidence Level {CL}')
#plt.plot(confidenceBelt_especial['tau'],confidenceBelt_especial['tau'],label='Real value')
plt.legend()
#plt.show()

# Guarda en un pdf todos los plots
fileName = 'MEFE2_TP2_AgustinRombola_Parte1.pdf'
pp = PdfPages(fileName)
pp.savefig(fig1)
pp.savefig(fig2)
pp.savefig(fig3)
pp.savefig(fig4)
pp.close()
print(f'Graficos guardados en un {fileName}')

##########################################
## PARTE D  
##########################################

def Likelihood_exp(x,t):
	beta = 1/t
	acum = 1
	for i in x:
		acum *= beta*np.exp(-i*beta)
	return acum

nPoints = 100
nPointsOfLikelihood = 1000
nTaus = 100
CL = 0.68
taus = np.linspace(0,10,nTaus,endpoint=True)
taus = taus[1:] #Saco el CERO
figNum = 0
figs = []
confidence_belt = {'tau':taus,'tmin':[],'tmax':[]}
for tau in taus:
	print(f'Calculando para un tau = {tau} con Likelihood sin Montecarlo')
	x = []
	for i in range(nPoints):
		x.append(np.random.exponential(scale=tau))

	t_hat = np.mean(x)
	ll_t_hat = Likelihood_exp(x,t_hat)

	t = np.linspace(0,tau*2,nPointsOfLikelihood)
	t = t[1:] #Saco el CERO
	# Genera los valores para simular una likelihood
	y = []
	for i in t:
		y.append(Likelihood_exp(x,i))

	#Busca cuanto da la integral total para poder normalizar
	acum_total = 0
	for i in range(len(t)-1):
		acum_total += (t[i+1]-t[i])*y[i]
	#Busca t min y tmax
	acum = 0
	i = 0
	while ((acum/acum_total)<(1-CL)/2):
		acum += (t[i+1]-t[i])*y[i]
		i +=1
	tmin = t[i]
	acum = 0
	i = 0
	while ((acum/acum_total)<(1-(1-CL)/2)):
		acum += (t[i+1]-t[i])*y[i]
		i +=1
	tmax = t[i]
	confidence_belt['tmin'].append(tmin)
	confidence_belt['tmax'].append(tmax)
	fig_aux = 0
	fig_aux = plt.figure(figNum)
	fig_aux.clf()
	plt.title(f'Likelihood para tau = {tau}')
	plt.plot(t,y/ll_t_hat,label='Likelihood')
	plt.axvline(x=t_hat,label='Max Likelihood con t = mean',color='green')
	plt.axvline(x=tmin,label='tmin',color='red')
	plt.axvline(x=tmax,label='tmax',color='red')

	plt.xlim([0,tau*2])
	plt.legend()
	#plt.show()
	figs.append(fig_aux)
	figNum += 1

fig_final = plt.figure(5000)
plt.title('Confidence Belt con Wilks sin Montecarlo')
plt.plot(confidence_belt['tau'],confidence_belt['tmin'],color='red',label='tMin')
plt.plot(confidence_belt['tau'],confidence_belt['tmax'],color='red',label='tMan')
plt.fill_between(confidence_belt['tau'],confidence_belt['tmin'],confidence_belt['tmax'],color='#66CC00',label=f'Confidence Level {CL}')
plt.plot(confidence_belt['tau'],confidence_belt['tau'],label='Real value')
plt.xlim([0,10])
plt.legend()
#plt.show()

fileName = 'MEFE2_TP2_AgustinRombola_Parte2.pdf'
pp = PdfPages(fileName)

pp.savefig(fig_final)
for f in figs:
	pp.savefig(f)

pp.close()
print(f'Graficos guardados en un {fileName}')
