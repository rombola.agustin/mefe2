# Métodos estadísticos avanzados en tratamiento de datos - 2do Cuatrimestre 2021

## Notas del 23 de noviembre de 2021

TH1:Fit por default hace *chisquare* (Neyman) pero si no se puede setear como parámetro para hacer el fit:

- "L": Use loglikelihood method 
- "P": Pearson chi2 
- "E": Perform better Errors estimation using Minos technique.

Código de colores:

**N --> Rojo**

**P --> Azul**

**L --> Verde**

Si se hace un histograma con 40 repiticiones de generar números random con una distribución exponencial con parámetro 2. Se puede ajustar el histograma con cualquiera de las 3 formas: **Neyman, Pearson, LogLikelihood** Se obtiene 3 tau distintos como estimadores con su respectivos errores. Una forma de obtener tau con mayor precisión sería realizar n repeticiones del experimento "Obtener 40 números al azar con dist exp" y hacer un histograma de los taus. De esa forma se puede ver cuál es el valor de tau y su sigma. 

Si el experimentos tiene n -> inf de números, los 3 taus convergen a 2 y el error de cada uno de ellos a cero.

La distribución de la variable aleatoria "Caer en un determinado bin" es Poisson donde la esperanza es el valor esperado del bin. Por ejemplo si estas haciendo una distribución aleatoria uniforme con 1000 datos,entre 0 y 100 espero que cada bin tenga una altura de 10.



