// Este Scripts compara la performance de Bootstrap contra Toys Montecarlo
// Ayuda a visualizar que tamaño de muestra y número de réplicas son suficientes
// para obtener una estimación razonable de la incerteza en el promedio de una muestra Poissoniana

void bootstrap_vs_toy(){
	
	int sample_size=20;
	const int replicas=100;
	
	double mu=50;
	double sigma = sqrt(50);
	double sigmam = sigma/sqrt(sample_size);
	int emin=mu-3*sigma;
	int emax=mu+3*sigma;
	int nsigmas=10;
	 	
	int bines = (emax-emin);
	
    vector<double> population; 
    vector<double> sample;  
    vector<double> bootstrap_sample; 
    double means[3] = {0};
    double std[3] = {0};
    double samples[3] = {0};
	TRandom3 X_Random_variable(0); 

////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////
    gROOT->SetStyle("Plain");
    TCanvas * canvas = new TCanvas("canvas","canvas",2000,1000);
    canvas->Divide(4,3);
    int cf = 0;
for (int tries = 1; tries < 4; tries++){
	sample_size = 20*tries;


////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////

    //canvas->cd(cf);
    //canvas->cd(cf)->SetGridx();
    //canvas->cd(cf)->SetGridy();
    
    TF1 *pdf = new TF1("pdf","TMath::Poisson(x,50)",emin,emax);
    //pdf->SetLineColor(kRed);
    //pdf->Draw();


////////////////////////////////////////////////////////////////////////////////////////////////////
// Aca empiezo los Toys
		
	//TH1D * histo_toy  =  new TH1D("histo_toy","", bines, mu-nsigmas*sigmam, mu+nsigmas*sigmam);
	TH1D * histo_toy  =  new TH1D("histo_toy","", bines, pow(mu-nsigmas*sigmam,2), pow(mu+nsigmas*sigmam,2)); //promedio al cuadrado
	histo_toy -> Sumw2();
	cf +=1;
    canvas->cd(cf);
    canvas->cd(cf)->SetGridx();
    canvas->cd(cf)->SetGridy();

////////////////////////////////////////////////////////////////////////////////////////////////////
// Hago los toys   
   
	double toys_mean;
    for (int i = 1; i < replicas+1; ++i) {
		double t_sample;
		double t_sample_suma;
		
		for (int j = 1; j < sample_size+1; ++j) {
			t_sample = X_Random_variable.Poisson(mu);
			t_sample_suma+= t_sample;
		}
		double mean=double(t_sample_suma)/sample_size;
		t_sample_suma=0;
		//double statistic=mean;
		double statistic = pow(mean,2); // new statistic
		toys_mean+=mean;
		histo_toy->Fill(statistic);
    }
    toys_mean=toys_mean/replicas;
    //cout<<"El promedio de los toys es:   "<<toys_mean<<endl;
    toys_mean=pow(toys_mean,2);
    cout<<"El promedio al cuadrado de los toys es:   "<<toys_mean<<endl;
    histo_toy->SetTitle("Toys del promedio");
    histo_toy->GetXaxis()->SetTitle("promedio de la replica");
	histo_toy->GetYaxis()->SetTitle("#");
	histo_toy->SetLineWidth(2);

	histo_toy->Draw("E0 HIST");
	
////////////////////////////////////////////////////////////////////////////////////////////////////
// Aca empiezo el bootstrap
	
////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////
	
	TH1D * histo_sample  =  new TH1D("histo_sample","", bines, emin, emax);
	histo_sample -> Sumw2();
	cf += 1;
    canvas->cd(cf);
    canvas->cd(cf)->SetGridx();
    canvas->cd(cf)->SetGridy();

    
////////////////////////////////////////////////////////////////////////////////////////////////////
// Tomo la muestra 
	
	double x_sample_mean;
    for (int i = 1; i < sample_size+1; ++i) {
		double x_sample;
		//x_sample=X_Random_variable.Uniform(emin,emax);
		x_sample=X_Random_variable.Poisson(mu);
		x_sample_mean+=x_sample;
		sample.push_back(x_sample);
		histo_sample->Fill(x_sample);
    }
    
    x_sample_mean=double(x_sample_mean)/sample_size;
    // promedio al cudrado:
    cout<<"El promedio de la muestra es: "<<x_sample_mean<<endl;
    x_sample_mean = pow(x_sample_mean,2);
    cout<<"El promedio al cuadrado de la muestra es: "<<x_sample_mean<<endl;
   
    histo_sample->SetTitle("Muestra");
    histo_sample->GetXaxis()->SetTitle("realizacion");
	histo_sample->GetYaxis()->SetTitle("#");
	histo_sample->SetLineWidth(2);
	/*
	if (tries==1){
		histo_sample->SetLineColor(kBlue);
	}
	if (tries==2){
		histo_sample->SetLineColor(kRed);
	}
	if (tries==3){
		histo_sample->SetLineColor(kGreen);
	}
	*/
	histo_sample->SetLineColor(kBlue);
	histo_sample->Draw("E0 HIST");
	//histo_sample->Draw("SAME");
	
////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////
		
	//TH1D * histo_bootstrap  =  new TH1D("histo_bootstrap","", bines, mu-nsigmas*sigmam, mu+nsigmas*sigmam);
	TH1D * histo_bootstrap  =  new TH1D("histo_bootstrap","", bines, pow(mu-nsigmas*sigmam,2), pow(mu+nsigmas*sigmam,2));
	histo_bootstrap -> Sumw2(); //Error del histograma 
	cf += 1;
    canvas->cd(cf);
    canvas->cd(cf)->SetGridx();
    canvas->cd(cf)->SetGridy();
////////////////////////////////////////////////////////////////////////////////////////////////////
// Hago las replicas, estas son con reposicion
	double bootstrap_mean[replicas] = {0};
	double bootstrap_std[replicas] = {0};
	double number_replicas[replicas] = {0};
	for (int r = 10; r < replicas+1;++r){
    for (int i = 1; i < r+1; ++i) {
		double b_sample;
		double b_sample_suma;

		for (int j = 1; j < sample_size+1; ++j) {
			int indice=X_Random_variable.Uniform(1,sample_size);
			b_sample=sample[indice];
			b_sample_suma+=b_sample;
		}
		double mean=b_sample_suma/sample_size;
		b_sample_suma=0;
		double statistic = pow(mean,2); // New statistic
		bootstrap_sample.push_back(statistic);
		histo_bootstrap->Fill(statistic);
    }
    cout << r << "\n";
    bootstrap_mean[r-9] = histo_bootstrap->GetMean();
    bootstrap_std[r-9] = histo_bootstrap->GetStdDev();
    number_replicas[r-9] = r;
	}
	
    canvas->cd(cf);
    canvas->cd(cf)->SetGridx();
    canvas->cd(cf)->SetGridy();
   	TGraph* canvas1 = new TGraph(replicas-10,number_replicas,bootstrap_std);
   	canvas1->SetTitle("STD vs Replicas");
    canvas1->GetXaxis()->SetTitle("# Replicas");
	canvas1->GetYaxis()->SetTitle("STD");
   	canvas1->Draw("AC");
   	cf +=1;
   	canvas->cd(cf);
   	canvas->cd(cf)->SetGridx();
    canvas->cd(cf)->SetGridy();
   	TGraph* canvas2 = new TGraph(replicas-10,number_replicas,bootstrap_mean);
   	canvas2->SetTitle("Mean vs Replicas");
    canvas2->GetXaxis()->SetTitle("# Replicas");
	canvas2->GetYaxis()->SetTitle("Mean");
   	canvas2->Draw("AC");
    /*

    bootstrap_mean=bootstrap_mean/replicas;
    cout<<"El promedio de las replicas es:   "<<bootstrap_mean<<endl;
    double bias=abs(bootstrap_mean-x_sample_mean);
    cout<<"por lo tanto el bias es:          "<<bias<<endl;
    
    histo_bootstrap->SetTitle("Bootstrap para el promedio");
    histo_bootstrap->GetXaxis()->SetTitle("promedio de la replica");
	histo_bootstrap->GetYaxis()->SetTitle("#");
	histo_bootstrap->SetLineWidth(2);
	histo_bootstrap->SetLineColor(kRed);
	
	histo_bootstrap->Draw("E0 HIST");
	//histo_bootstrap->Draw("E0 HIST");
	*/
////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////


}
}
