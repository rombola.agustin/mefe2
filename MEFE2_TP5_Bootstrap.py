###################################################################################################################
#
# TP 5 - PARTE B - AGUSTIN ROMBOLA
#
###################################################################################################################

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages

###################################################################################################################
############################ FUNCIONES ############################################################################
###################################################################################################################

# Calcula la correlacion
def correlation(f1,f2):
	f1 = np.array(f1)
	f2 = np.array(f2)
	n = len(f1)
	numerador = np.sum((f1-np.ones(n)*np.mean(f1))*(f2-np.ones(n)*np.mean(f2)))
	denominador = np.sum((f1-np.ones(n)*np.mean(f1))**2)*np.sum((f2-np.ones(n)*np.mean(f2))**2)
	#print(numerador)
	#print(np.sqrt(denominador))
	if denominador == 0:
		rho = 0
	else:
		rho = numerador/np.sqrt(denominador)
	return rho

###################################################################################################################
############################ MAIN #################################################################################
###################################################################################################################

#Cargo los fotones del .txt
data = np.loadtxt('fotones36.txt')
fotones = []
for i in data:
	fotones.append(i[1])

# Se define el numero de particiones
nParticiones = len(fotones)
rho_poblacion = []
angle = []
index_angle = []
nBootstrap = 2000 #Repeticiones del bootstrap
rhoBoot = {}
sampleSize = len(fotones) #Size de las muestras de bootstrap, las elijo del mismo tamaño original
correlacion_poblacion = []
#Barra hasta n-1 para no contar la rotacion de 360
for i in range(nParticiones-1):
	i +=1 #Para que no cuenta la rotacion con angulo 0
	f1 = fotones
	f2 = fotones[i:] + fotones[0:i]
	angleRotation = (i*(360/nParticiones)) # In degrees
	angle.append(angleRotation)	

	print(f'Rotacion con un angulo de {angleRotation} grados')
	index_angle.append(str(angleRotation))
	rho_poblacion.append(correlation(f1,f2))

	rhoBoot[str(angleRotation)] = []

	#Hago el bootstrap para esta comparación de f1 con f2
	for b in range(nBootstrap):
		mBoot1 = []
		mBoot2 = []
		for sample in range(sampleSize):
			index = int(np.random.uniform(low=0,high=len(f1)))
			mBoot1.append(f1[index])
			mBoot2.append(f2[index])
		rhoBoot[str(angleRotation)].append(correlation(mBoot1,mBoot2))
	i += -1

###################################################################################################################
############################ GRAFICO ##############################################################################
###################################################################################################################

nBins = 40

meansBootstrap = []
stdsBootstrap = []

fileName = 'MEFE2_TP5_AgustinRombola_ParteB.pdf'
pp = PdfPages(fileName)
print('Graficando...')
#Primera pagina del PDF para guardar los graficos
cf = 1     #Auxiliar para el número de figuras
page1 =plt.figure(figsize=[8-1/4,11-3/4],dpi=200)
for i in index_angle[0:18]:
	plt.subplot(6,3,cf)
	meansBootstrap.append(np.mean(rhoBoot[i]))
	# Calculo el error por bootstrap
	var=0.0
	for j in range(0,nBootstrap):
		var+=np.power(rhoBoot[i][j]-np.mean(rhoBoot[i]),2)/(nBootstrap-1)
	stdsBootstrap.append(np.power(var,0.5))
	#Armo el histograma
	xHist = np.linspace(np.min(rhoBoot[i]),np.max(rhoBoot[i]),nBins)
	yHist,a = np.histogram(rhoBoot[i],xHist,density=False)
	widthBar = xHist[1]-xHist[0]
	#plt.bar(xHist[:-1],yHist,width=widthBar,edgecolor='black',linewidth=1)
	plt.xlim([-1,1])
	plt.grid(alpha=0.5)
	plt.bar(xHist[:-1],yHist,width=widthBar,color='green')
	plt.title(f'Rotacion {str(i)}')
	plt.ylabel('# Muestras')
	plt.xlabel('Correlacion')
	plt.tight_layout()
	#plt.legend()
	cf += 1
pp.savefig(page1)
print('La pagina 1 ha sido generada con exito')
#Segunda pagina del PDF para guardar los graficos
cf = 1 #Auxiliar para el número de figuras
page2 =plt.figure(figsize=[8-1/4,11-3/4],dpi=200)
for i in index_angle[18:]:
	plt.subplot(6,3,cf)
	meansBootstrap.append(np.mean(rhoBoot[i]))
	# Calculo el error por bootstrap
	var=0.0
	for j in range(0,nBootstrap):
		var+=np.power(rhoBoot[i][j]-np.mean(rhoBoot[i]),2)/(nBootstrap-1)
	stdsBootstrap.append(np.power(var,0.5))
	#Armo el histograma
	xHist = np.linspace(np.min(rhoBoot[i]),np.max(rhoBoot[i]),nBins)
	yHist,a = np.histogram(rhoBoot[i],xHist,density=False)
	widthBar = xHist[1]-xHist[0]
	#plt.bar(xHist[:-1],yHist,width=widthBar,edgecolor='black',linewidth=1)
	plt.xlim([-1,1])
	plt.grid(alpha=0.5)
	plt.bar(xHist[:-1],yHist,width=widthBar,color='green')
	plt.title(f'Rotacion {str(i)}')
	plt.ylabel('#')
	plt.xlabel('Correlacion')
	plt.tight_layout()
	#plt.legend()
	cf += 1
pp.savefig(page2)
print('La pagina 2 ha sido generada con exito')
#Tercer pagina del PDF para guardar los graficos
page3 =plt.figure(figsize=[8-1/4,11-3/4],dpi=200)

plt.subplot(2,1,1)
plt.title('Correlación de la poblacion para cada rotación')
plt.plot(angle,rho_poblacion)
plt.ylabel('Correlacion')
plt.xlabel('Angulo')

plt.subplot(2,1,2)
plt.title('Correlación para cada rotación calculada con Bootstrap')
plt.ylabel('Correlacion')
plt.xlabel('Angulo')
plt.errorbar(x=angle,y=meansBootstrap,yerr=stdsBootstrap,ecolor='red', capsize=4)
#print(f'Error calculado con bootstrap: {stdsBootstrap}')
plt.tight_layout()
pp.savefig(page3)
print('La pagina 3 ha sido generada con exito')

pp.close()
print(f'Graficos guardados en un {fileName}')
