
void test_TF1(double xmin = 0, double xmax = 10){

TF1 f1("f1","[0]*x+[1]",xmin,xmax);
f1.SetParameters(1,1);

double a;
double b;
double c;
double d;

a = f1.Eval(2);
cout << a << "\n";

b = f1.GetX(5,0,10);
cout << b << "\n";

c = f1.Integral(0,10);
cout << c << "\n";

d = f1.GetMaximum();
cout << d << "\n";

//f1.Draw();
}