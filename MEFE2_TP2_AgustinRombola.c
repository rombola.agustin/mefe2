// ************************
//
// TP 2 : Agustin Rombola
//
// ************************

void MEFE2_TP2_AgustinRombola(int nToys=1000,int nPoints = 10) {

float tau = 5;
double tMean;
vector<double> means(nToys);

for(int j = 0; j < nToys; ++j) {
	
	vector<double> x(nPoints);

	//Genero los numeros al azar con distribucion exponencial 
	TRandom3 r(0);    // En cero para random numbers
	for (int i = 0; i < nPoints; ++i) x[i] = r.Exp(tau);

	/*
	// Genero el histograma h1

	auto h1 = new TH1D("h1","Generated Exponential data",20,0,10*tau);

	// Lleno el histograma
	for (auto x_i : x)  h1->Fill(x_i);

	// Dibujo el histograma
	h1->Draw();
	*/

	// Calculo el promedio
	tMean = 0;
	for (auto x_i : x) tMean=tMean+x_i;
	//cout << tMean;
	means[j] = tMean/nPoints;
}
auto h2 = new TH1D("h2","Mean",40,0,15);
for (auto mean_i : means)  h2->Fill(mean_i);
h2->Draw();
}