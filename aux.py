import numpy as np
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages

def Likelihood_exp(x,t):
	beta = 1/t
	acum = 1
	for i in x:
		acum *= beta*np.exp(-i*beta)
	return acum

nPoints = 100
nPointsOfLikelihood = 1000
nTaus = 100
CL = 0.68
taus = np.linspace(0,10,nTaus,endpoint=True)
taus = taus[1:] #Saco el CERO
figNum = 0
figs = []
confidence_belt = {'tau':taus,'tmin':[],'tmax':[]}
for tau in taus:
	print(f'Calculando para un tau = {tau}')
	x = []
	for i in range(nPoints):
		x.append(np.random.exponential(scale=tau))

	t_hat = np.mean(x)
	ll_t_hat = Likelihood_exp(x,t_hat)

	t = np.linspace(0,20,nPointsOfLikelihood)
	t = t[1:] #Saco el CERO
	# Genera los valores para simular una likelihood
	y = []
	for i in t:
		y.append(Likelihood_exp(x,i))

	#Busca cuanto da la integral total para poder normalizar
	acum_total = 0
	for i in range(len(t)-1):
		acum_total += (t[i+1]-t[i])*y[i]
	#Busca t min y tmax
	acum = 0
	i = 0
	while ((acum/acum_total)<(1-CL)/2):
		acum += (t[i+1]-t[i])*y[i]
		i +=1
	tmin = t[i]
	acum = 0
	i = 0
	while ((acum/acum_total)<(1-(1-CL)/2)):
		acum += (t[i+1]-t[i])*y[i]
		i +=1
	tmax = t[i]
	confidence_belt['tmin'].append(tmin)
	confidence_belt['tmax'].append(tmax)
	fig_aux = 0
	fig_aux = plt.figure(figNum)
	plt.title(f'Likelihood para tau = {tau}')
	plt.plot(t,y/ll_t_hat,label='Likelihood')
	plt.axvline(x=t_hat,label='Max Likelihood con t = mean',color='green')
	plt.axvline(x=tmin,label='tmin',color='red')
	plt.axvline(x=tmax,label='tmax',color='red')
	plt.legend()
	#plt.show()
	figs.append(fig_aux)
	figNum += 1

fig_final = plt.figure(figNum+1)
plt.title('Confidence Belt con Wilks sin Montecarlo')
plt.plot(confidence_belt['tau'],confidence_belt['tmin'],color='red',label='tMin')
plt.plot(confidence_belt['tau'],confidence_belt['tmax'],color='red',label='tMan')
plt.fill_between(confidence_belt['tau'],confidence_belt['tmin'],confidence_belt['tmax'],color='#66CC00',label=f'Confidence Level {CL}')
plt.plot(confidence_belt['tau'],confidence_belt['tau'],label='Real value')
plt.xlim()
plt.legend()
#plt.show()

fileName = 'Likelihood_without_Montecarlo.pdf'
pp = PdfPages(fileName)

pp.savefig(fig_final)
for f in figs:
	pp.savefig(f)

pp.close()
print(f'Graficos guardados en un {fileName}')