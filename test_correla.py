import numpy as np
def correlation(f1,f2):
	f1 = np.array(f1)
	f2 = np.array(f2)
	n = len(f1)
	numerador = np.sum((f1-np.ones(n)*np.mean(f1))*(f2-np.ones(n)*np.mean(f2)))
	denominador = np.sum((f1-np.ones(n)*np.mean(f1))**2)*np.sum((f2-np.ones(n)*np.mean(f2))**2)
	print(numerador)
	print(np.sqrt(denominador))
	if denominador == 0:
		rho = 0
	else:
		rho = numerador/np.sqrt(denominador)
	return rho

f1 = [1,2,3,4,5,6]
f2 = [-1,-2.5,-3,-4,-4.5,-6]

print(correlation(f1,f2))
