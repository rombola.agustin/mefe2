void TestPoisson(){
	double nObs = 5;
    TF1 Posterior("f","(1/[1])*(exp(-x)*pow(x,[0]))",0,100);
    //TF1 Posterior("f","TMath::Gaus([0],x,sqrt(x),kTRUE)");
    //Posterior.SetNpx(200);
    Posterior.SetParameter(0,nObs);
    Posterior.SetParameter(1,1);
    double area = Posterior.Integral(0,100);
    Posterior.SetParameter(1,area);
    double test = Posterior.Integral(0,100);

    double xmin[1];
    double xmax[1];
    double qmin[1]={0.16};
    double qmax[1]={0.84};
    Posterior.GetQuantiles(1,xmin,qmin);
    Posterior.GetQuantiles(1,xmax,qmax);
   

    cout << area << '\n';
    cout << test << '\n';

    cout << xmin[0] << '\n';
    cout << xmax[0] << '\n';

    double integral1 = Posterior.Integral(0,xmin[0]);
    cout << integral1 << '\n';

    double integral2 = Posterior.Integral(0,xmax[0]);
    cout << integral2 << '\n';
}